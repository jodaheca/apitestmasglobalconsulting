package co.api.service.impl;

import co.api.dto.EmployeeDTO;
import co.api.dto.EmployeeHourlySalaryDTO;
import co.api.dto.EmployeeMgcDTO;
import co.api.dto.EmployeeMonthlySalaryDTO;
import co.api.repository.IEmployeerepository;
import co.api.util.IFactoryEmployee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService employeeService;

    @Mock
    private IEmployeerepository employeeRepository;
    @Mock
    private IFactoryEmployee factoryEmployee;

    private List<EmployeeMgcDTO> listEmployeeMgcDTO;
    private EmployeeMgcDTO employeeMgcDTO1;
    private EmployeeMgcDTO employeeMgcDTO2;
    private EmployeeMonthlySalaryDTO employeeMonthlySalaryDTO;
    private EmployeeHourlySalaryDTO employeeHourlySalaryDTO;

    @Before
    public void setup(){
        listEmployeeMgcDTO = new ArrayList<>();
        employeeMgcDTO1 = EmployeeMgcDTO.builder()
                .id("1")
                .name("Juan")
                .contractTypeName("HourlySalaryEmployee")
                .roleId("1")
                .roleDescription(null)
                .hourlySalary(60000.0)
                .monthlySalary(80000.0).build();

        employeeMgcDTO2 = EmployeeMgcDTO.builder()
                .id("2")
                .name("Sebastian")
                .contractTypeName("MonthlySalaryEmployee")
                .roleId("2")
                .roleDescription(null)
                .hourlySalary(60000.0)
                .monthlySalary(80000.0).build();

        listEmployeeMgcDTO.add(employeeMgcDTO1);
        listEmployeeMgcDTO.add(employeeMgcDTO2);

        employeeHourlySalaryDTO = new EmployeeHourlySalaryDTO(employeeMgcDTO1);
        employeeMonthlySalaryDTO = new EmployeeMonthlySalaryDTO(employeeMgcDTO2);

        Mockito.when(employeeRepository.getAll()).thenReturn(listEmployeeMgcDTO);
        Mockito.when(factoryEmployee.getEmployee(employeeMgcDTO1)).thenReturn(employeeHourlySalaryDTO);
        Mockito.when(factoryEmployee.getEmployee(employeeMgcDTO2)).thenReturn(employeeMonthlySalaryDTO);

    }

    @Test
    public void test_getAllSuccess(){

        List<EmployeeDTO> result = employeeService.getAll();

        assertTrue(result.size() == 2);
        assertEquals(8.64E7,result.get(0).getAnnualSalary(),0 );
        assertEquals(960000.0,result.get(1).getAnnualSalary(),0 );
    }

    @Test
    public void test_getOneEmployeeSuccess(){

        EmployeeDTO result = employeeService.getOne("1");

        assertEquals("Juan", result.getName());
        assertEquals(8.64E7,result.getAnnualSalary(),0 );
    }

    @Test
    public void test_getOneEmployeeDoesNotExistEmployee(){

        assertEquals(null, employeeService.getOne("10"));
    }

}