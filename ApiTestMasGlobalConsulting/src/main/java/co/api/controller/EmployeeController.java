package co.api.controller;


import co.api.dto.EmployeeDTO;
import co.api.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE + ";charset=utf-8")
    public List<EmployeeDTO> getAll() {
        return employeeService.getAll();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE + ";charset=utf-8")
    public EmployeeDTO getOne(@PathVariable String id) {
        return employeeService.getOne(id);
    }
}
