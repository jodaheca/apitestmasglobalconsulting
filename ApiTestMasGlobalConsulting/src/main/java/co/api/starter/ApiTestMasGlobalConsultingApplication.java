package co.api.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"co.api.*"})
@SpringBootApplication
public class ApiTestMasGlobalConsultingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTestMasGlobalConsultingApplication.class, args);
	}
}
