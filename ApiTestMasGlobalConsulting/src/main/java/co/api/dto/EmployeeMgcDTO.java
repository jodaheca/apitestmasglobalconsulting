package co.api.dto;

import lombok.Data;
import lombok.experimental.Builder;

/**
 * DTO to read the information from Mas Global Consulting service
 * @author Joaquin Hernández
 */
@Data
@Builder
public class EmployeeMgcDTO {
    protected String id;
    protected String name;
    protected String contractTypeName;
    protected String roleId;
    protected String roleDescription;
    protected Double hourlySalary;
    protected Double monthlySalary;
}
