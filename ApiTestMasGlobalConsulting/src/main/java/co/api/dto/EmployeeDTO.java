package co.api.dto;

import lombok.Data;

@Data
public abstract class EmployeeDTO {
    public static final String HourlySalaryEmployee = "HourlySalaryEmployee";

    protected String id;
    protected String name;
    protected String contractTypeName;
    protected String roleId;
    protected String roleDescription;
    protected Double hourlySalary;
    protected Double monthlySalary;
    protected Double annualSalary;


    public abstract void calculateAnnualSalary();

}
