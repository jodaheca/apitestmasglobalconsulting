package co.api.dto;

import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
public class HttpResponse {
    private int status;
    private String bodyResponse;
}
