package co.api.dto;

public class EmployeeHourlySalaryDTO extends EmployeeDTO {

    public EmployeeHourlySalaryDTO (EmployeeMgcDTO employeeInfo){
        this.id = employeeInfo.getId();
        this.name = employeeInfo.getName();
        this.contractTypeName = employeeInfo.getContractTypeName();
        this.roleId = employeeInfo.getRoleId();
        this.roleDescription = employeeInfo.getRoleDescription();
        this.hourlySalary = employeeInfo.getHourlySalary();
        this.monthlySalary = employeeInfo.getMonthlySalary();
    }

    @Override
    public void calculateAnnualSalary() {
         annualSalary = 120 * hourlySalary * 12;
    }
}
