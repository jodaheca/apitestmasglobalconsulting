package co.api.util.impl;

import co.api.dto.EmployeeDTO;
import co.api.dto.EmployeeHourlySalaryDTO;
import co.api.dto.EmployeeMgcDTO;
import co.api.dto.EmployeeMonthlySalaryDTO;
import co.api.util.IFactoryEmployee;
import org.springframework.stereotype.Component;

@Component
public class FactoryEmployee implements IFactoryEmployee {


    @Override
    public EmployeeDTO getEmployee(EmployeeMgcDTO employeeInformation) {
        EmployeeDTO employee;
        if (EmployeeDTO.HourlySalaryEmployee.equalsIgnoreCase(employeeInformation.getContractTypeName())){
            employee = new EmployeeHourlySalaryDTO(employeeInformation);
        }else{
            employee = new EmployeeMonthlySalaryDTO(employeeInformation);
        }
        return employee;
    }
}
