package co.api.util.impl;

import co.api.dto.HttpResponse;
import co.api.util.IHttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Component
public class HttpRequest implements IHttpRequest {

    @Override
    public HttpResponse httpGetRequest(String urlService) throws MalformedURLException, java.io.IOException {

        String finalResponse = "";
        int status;
        URL url = new URL(urlService);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);

        status = con.getResponseCode();

        if (status == HttpStatus.OK.value()){
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            con.disconnect();
            finalResponse = response.toString();
        }

        return HttpResponse.builder().status(status).bodyResponse(finalResponse).build();
    }
}
