package co.api.util;

import co.api.dto.HttpResponse;

import java.net.MalformedURLException;

/**
 * Manage the request to external services
 * @author Joaquin Hernández
 */
public interface IHttpRequest {

    public HttpResponse httpGetRequest(String url)throws MalformedURLException, java.io.IOException;
}
