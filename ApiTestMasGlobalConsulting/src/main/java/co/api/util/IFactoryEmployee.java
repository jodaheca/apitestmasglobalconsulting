package co.api.util;

import co.api.dto.EmployeeDTO;
import co.api.dto.EmployeeMgcDTO;

/**
 * factory to {@link co.api.dto.EmployeeHourlySalaryDTO} and {@link co.api.dto.EmployeeMonthlySalaryDTO}
 * @author Joaquin Hernández
 */
public interface IFactoryEmployee {

    EmployeeDTO getEmployee(EmployeeMgcDTO employee);
}
