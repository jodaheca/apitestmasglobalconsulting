package co.api.repository;

import co.api.dto.EmployeeMgcDTO;

import java.util.List;

public interface IEmployeerepository {

    List<EmployeeMgcDTO> getAll();
}
