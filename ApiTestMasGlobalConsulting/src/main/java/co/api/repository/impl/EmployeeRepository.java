package co.api.repository.impl;

import co.api.dto.EmployeeDTO;
import co.api.dto.EmployeeMgcDTO;
import co.api.dto.HttpResponse;
import co.api.repository.IEmployeerepository;
import co.api.util.IHttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class EmployeeRepository implements IEmployeerepository {

    private static final String URL_EMPLOYEE_SERVICE = "http://masglobaltestapi.azurewebsites.net/api/employees";

    @Autowired
    private IHttpRequest httpRequest;

    @Override
    public List<EmployeeMgcDTO> getAll() {

        HttpResponse response = null;
        List<EmployeeMgcDTO> employees = new ArrayList<>();
        try {
            response = httpRequest.httpGetRequest(URL_EMPLOYEE_SERVICE);
            if (response.getStatus() == 200){
                Gson gson = new Gson();
                Type type = new TypeToken<List<EmployeeMgcDTO>>() {}.getType();
                employees = gson.fromJson(response.getBodyResponse(), type);
            }
        } catch (IOException e) {
            log.error("Error consuming the employee service (MAS GLOBAL CONSULTING)",e);
        }
        return employees;
    }
}
