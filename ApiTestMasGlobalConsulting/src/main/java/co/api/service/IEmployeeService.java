package co.api.service;

import co.api.dto.EmployeeDTO;

import java.util.List;

public interface IEmployeeService {

    List<EmployeeDTO> getAll();

    EmployeeDTO getOne(String id);
}
