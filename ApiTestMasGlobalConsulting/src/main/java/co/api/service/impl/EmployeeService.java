package co.api.service.impl;

import co.api.dto.EmployeeDTO;
import co.api.dto.EmployeeMgcDTO;
import co.api.repository.IEmployeerepository;
import co.api.service.IEmployeeService;
import co.api.util.IFactoryEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    private IEmployeerepository employeeRepository;
    @Autowired
    private IFactoryEmployee factoryEmployee;

    @Override
    public List<EmployeeDTO> getAll() {

        List<EmployeeMgcDTO> infoEmployees = employeeRepository.getAll();
        List<EmployeeDTO> employeesList = new ArrayList<>();

        infoEmployees.forEach(employeeInformation -> {
            EmployeeDTO employeeDTO = factoryEmployee.getEmployee(employeeInformation);
            employeeDTO.calculateAnnualSalary();
            employeesList.add(employeeDTO);
        });
        return employeesList;
    }

    @Override
    public EmployeeDTO getOne(String id) {
        List<EmployeeMgcDTO> infoEmployees = employeeRepository.getAll();
        EmployeeDTO employee = null;
        infoEmployees.removeIf(employeeMgcDTO -> !employeeMgcDTO.getId().equalsIgnoreCase(id));
        if (!infoEmployees.isEmpty()){
            employee = factoryEmployee.getEmployee(infoEmployees.get(0));
            employee.calculateAnnualSalary();
        }
        return employee;
    }
}
